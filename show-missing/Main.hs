module Main (main) where

import Pack
import Equiv

import Control.Lens ( (%~) )
import Control.Lens.Tuple
import Control.Monad
import Control.Monad.Extra
import System.Directory
import System.FilePath
import qualified Data.Map as M
import Data.Maybe


main :: IO ()
main = do mm <- missingImageMap
          -- fm <- filterM (doesFileExist . fst) $ mm & traverse._1 %~ captureFile
          -- let xfm = mm & traverse._1 %~ captureFile
          -- traverse_ print xfm
          writeFile "missing.html" $ unlines $ htmlWrap $ mconcat [htmlForMissingFile ipath lnks | (ipath, lnks) <- mm]
          -- pure ()

htmlForMissingFile :: FilePath -> [FilePath] -> [String]
htmlForMissingFile ipath lnks = ["<tr><td><img src=\"" ++ ipath ++ "\" style=\"width:128px\">",
                                 "<td>" ++ ipath,
                                 "<td>"] ++ htmlForLinks lnks

htmlForLinks :: [FilePath] -> [String]
htmlForLinks paths = ["<table>"] ++ fmap ("<tr><td>" ++) paths ++ ["</table>"]

htmlWrap :: [String] -> [String]
htmlWrap ls = ["<html>","<body>","<table>"] ++ ls ++ ["<table>","</body>", "</html>"]

-- For referenced but missing files list the texture subareas with the list of
-- link files that ended in the missing reference
missingImageMap :: IO [(FilePath, [FilePath])]
missingImageMap = withCurrentDirectory "pack" $ do
    packFiles <- parsePackFiles
    let equivs = [e | PackEquiv e <- packFiles]
    let maps = [m | PackMap m <- packFiles]
    lrs <- mconcat <$> traverse linkRedirectionsForCat maps
    missing <- filterM (fmap not . doesFileExist . last . snd) lrs
    captureMap <- equivToCaptureMap $ combineEquivs equivs
    pure $ traverse._1 %~ (\tsa -> case M.lookup tsa captureMap of
                                        Nothing -> captureFile tsa
                                        Just p -> p
                          ) $ missing


captureFile :: TexSubarea -> FilePath
captureFile (h, tr) = "Capture" </> h </> texRectToString tr <.> "bmp"

linkRedirectionsForCat :: MapCat -> IO [(TexSubarea, [FilePath])]
linkRedirectionsForCat (MapCat root maps) = mconcat <$> traverse (linkRedirectionsForHashPath root) maps

equivElementToCaptureMap :: [TexSubarea] -> IO (Maybe (M.Map TexSubarea FilePath))
equivElementToCaptureMap equivs =
    do (found, missing) <- partitionM (doesFileExist . captureFile) equivs
       pure $ if null found then Nothing
                            else Just $ M.fromList [(m, captureFile $ choose m found) | m <- missing]

equivToCaptureMap :: [[TexSubarea]] -> IO (M.Map TexSubarea FilePath)
equivToCaptureMap equiv = M.unions . catMaybes <$>  traverse equivElementToCaptureMap equiv

choose :: Eq a => a -> [a] -> a
choose a s = if a `elem` s then a else head s
