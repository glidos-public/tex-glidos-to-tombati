module Main (main) where

import Pack ( parsePack, TexRect(TexRect, xmax) )

import Control.Monad
import Control.Monad.State.Lazy
import Control.Lens.Tuple
import Data.Tuple.Extra
import System.FilePath
import System.Directory
import Data.Maybe
import Data.Char
import Data.List
import Data.Foldable
import Text.Regex.Applicative
import qualified Data.Map as M
import qualified MapExtra as M
import qualified Graphics.Image as G
import qualified Graphics.Image.Interface as G
import TImage
import MemoiseIO



outDir :: String
outDir = "out"


titleBits :: [[String]]
titleBits = [["8880FD9B651C257778549A7EE7A2E450", "E7504AFB7D3264410C74C2EA777AB1F3", "8F2FA7061AB2E84279F2292FDD537C46"],
              ["00AE19B7C0B2D045AE54A1683C076786", "9DB0846B3F90F68744D01237BFCE59CB", "C1135A3AA842C8E3CDD2493E48CFAE7F"]]


main :: IO ()
main = do
          texsubToPath <- parsePack
          putStrLn "Loading images ..."
          texsubToImage <- M.catMaybes <$> memoiseEval (traverse (memoise readImage.("pack"</>)) texsubToPath)
          putStrLn "... image loading complete."
          -- Reorganise to make a two-level mapping hash -> rect -> image
          let hashToRectToImage = M.curry texsubToImage
          keyMap <- readKeyFiles
          -- Initialise the output directory
          removePathForcibly outDir
          createDirectory outDir
          -- Write the keys file
          writeFile (outDir </> "keys.txt") $ unlines  [h ++ " " ++ n ++ ".png" | (h,n) <- M.toList keyMap]
          -- Generate the title screen image
          generateTitleImage $ titleImageGetter hashToRectToImage
          -- Generate the texture files
          sequence_ $ M.mapWithKey (generateImageForHash keyMap) hashToRectToImage

titleImageGetter :: M.Map String (M.Map TexRect TImage) -> String -> IO (Maybe TImage)
titleImageGetter hashToRectToImage hash = do
    -- There should be exactly one image per title-screen hash, so we can ignore
    -- the TexRects when looking up the hashes
    let maybeImages = M.elems <$> M.lookup hash hashToRectToImage
    case maybeImages of
        Just images -> do
            unless (length images == 1) $ do
                putStrLn $ "Multiple title-screen images for hash: " ++ hash
            return $ listToMaybe images
        Nothing -> do
            putStrLn $ "Missing title-screen image for hash: " ++ hash
            return Nothing

generateTitleImage :: (String -> IO (Maybe TImage)) -> IO ()
generateTitleImage getImage = do
    -- Try looking up the images for all the hashes
    titleImages <- traverse sequence <$> (traverse.traverse) getImage titleBits
    case titleImages of
        Just images -> do
            -- Form a list of the title-screen hashes with their corresponding 3x2 rectangle coordinates
            let indexedImages = [(x,y,hash) | (y,row) <- zip [0..] images, (x,hash) <- zip [0..] row]
            -- Patch the images together
            let blank = blackImage 640 480
            writeImage (outDir </> "TITLEH.png") $ foldl (\targ (x,y,patch) -> G.superimpose (y*256,x*256) patch targ) blank indexedImages
        Nothing -> return ()

generateImageForHash :: M.Map String FilePath -> String -> M.Map TexRect TImage -> IO ()
generateImageForHash keyMap hash rectToImage =
    -- Check the hash is listed in the keyMap
    case M.lookup hash keyMap of
        Just path -> do
            -- load the captured image for the texture
            originalTexture <- readImage ("Capture" </> hash </> "Complete.bmp")
            case originalTexture of
                Just i -> do let initial = unpremulitply $ scaleImageBy 4 4 $ cyanToTransparent i
                             let final = foldl patchImage initial (M.toList rectToImage)
                             writeImage (outDir </> path <.> "png") final
                Nothing -> putStrLn $ "Failed to find Complete.bmp for hash: " ++ hash
        Nothing -> putStrLn $ "No key mapping found for hash: " ++ hash

readKeyFiles :: IO (M.Map String FilePath)
readKeyFiles = withCurrentDirectory "Capture" $ do
    capFiles <- filterM doesFileExist =<< listDirectory ""
    let keyFiles = filter (isJust . (=~ many anySym <> string "keys.txt")) capFiles
    M.unions <$> traverse readKeyFile keyFiles

readKeyFile :: FilePath -> IO (M.Map String FilePath)
readKeyFile path = do
    let reHash = mconcat $ replicate 32 (singleton <$> psym isHexDigit)
    let reKeyFileLine = (,) <$> (reHash <* sym ' ') <*> some anySym
    M.fromList . mapMaybe (=~ reKeyFileLine) . lines <$> readFile path
