module Main (main) where

import Pack
import Equiv
import System.Directory

-- Concatenating equivalence relations, in the list of list is not a valid way
-- to combine them, but doing so and then converting to and back from the
-- element-to-set mapping form does correctly dervice the combination
main :: IO ()
main = do
    equivs <- withCurrentDirectory "pack" parseEquivFiles
    writeEquiv "equiv.txt" $ combineEquivs equivs
