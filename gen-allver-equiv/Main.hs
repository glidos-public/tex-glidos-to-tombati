module Main (main) where

import Pack
import EquivAll

import Data.Maybe
import Data.List
import qualified Data.Map as M

-- Make an equiv file specifically for this pack, so that all possible textures from
-- other versions of TR or UB become mapped, but no textures in the existing pack
-- become unintentially equated.
main :: IO ()
main = do
    mf <- parseFileContents "<internal equiv file>" equivAll
    case mf of Just (PackEquiv eql) -> do pack <- parsePack
                                          let missing = findMissing eql $ M.keys pack
                                          print $ length <$> eql
                                          print $ length <$> missing
                                          writeEquiv "allverequiv.txt" missing
               _ -> putStrLn "Failed to read file"


-- Take a list of equivalences and see if we can make use of each set.
-- If all of a set is already mapped, we don't need it. If none of them
-- are already mapped, there's nothing useful we can do with it. If it
-- has some mapped and some not, make a new list that has all the unmapped
-- ones and just one mapped one.
findMissing :: Eq a => [[a]] -> [a] -> [[a]]
findMissing eql mapped = mapMaybe (unmappedPlusOneMapped mapped) eql

unmappedPlusOneMapped :: Eq a => [a] -> [a] -> Maybe [a]
unmappedPlusOneMapped mapped vals =
    let included = intersect mapped vals
        excluded = vals \\ mapped
        in if not (null included) && not (null excluded) then Just $ head included : excluded
                                                         else Nothing