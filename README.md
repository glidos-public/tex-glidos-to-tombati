# tex-glidos-to-tombati

This program takes Tombraider I texture packs written for Glidos and converts them to be used with TombATI.

### Usage

Place the program in a folder with the texture pack. Rename the pack to "pack". Run the program.