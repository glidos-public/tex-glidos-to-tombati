{-# LANGUAGE LambdaCase,TemplateHaskell #-}
module Pack
    ( parsePack,
      parsePackFiles,
      parseEquivFiles,
      parseFile,
      parseFileContents,
      writeEquiv,
      linkRedirectionsForHashPath,
      texRectToString,
      TexRect(..),
      TexSubarea,
      PackCat(..),
      MapCat(..),
    ) where

import Equiv
import System.Directory
import System.FilePath
import Control.Lens hiding (cons)
import Control.Lens.Tuple
import Control.Monad
import Control.Monad.Extra
import Control.Monad.State.Lazy
import Data.Maybe
import Data.Foldable
import Text.Regex.Applicative
import Text.Regex.Applicative.Common
import Data.Char
import Data.List
import Data.List.Extra
import qualified Data.Map as M

data TexRect = TexRect {xmin :: Int, xmax :: Int, ymin :: Int, ymax :: Int} deriving (Show, Eq, Ord)
type TexSubarea = (String, TexRect)
data HashPath = HashPath {mhash :: String, mpath :: FilePath} deriving (Show, Eq, Ord)
data FileLine = EquivHeader | BeginEquiv | EndEquiv | EquivElem TexSubarea
                | MapHeader | Root FilePath | Base FilePath | MapElem HashPath
                | Blank |Error String deriving (Show, Eq, Ord)

data MapCat = MapCat {mroot :: FilePath, mpaths :: [HashPath]}

type EquivCat = [[TexSubarea]]

data PackCat = PackEquiv EquivCat | PackMap MapCat

data EParseState = EParseState {_eOn      :: Bool,
                                _eCurrent :: [TexSubarea],
                                _eResult  :: [[TexSubarea]]}

data MParseState = MParseState {_mRoot   :: FilePath,
                                _mBase   :: FilePath,
                                _mResult :: [HashPath]}

makeLenses 'EParseState
makeLenses 'MParseState

initEParseState :: EParseState
initEParseState = EParseState False [] []

initMParseState :: MParseState
initMParseState = MParseState "" "" []

equivHeader :: String
equivHeader = "GLIDOS TEXTURE EQUIV"

mapHeader :: String
mapHeader = "GLIDOS TEXTURE MAPPING"

beginEquivString :: String
beginEquivString = "BeginEquiv"

endEquivString :: String
endEquivString = "EndEquiv"

-- Regex for matching the contents of link files
reLink :: RE Char FilePath
reLink = string "TLNK: " *> (normaliseFilePath <$> some (psym $ not . isSpace)) <* many (psym isSpace)

-- Regex for matching the equiv file header line
reEquivHeader :: RE Char FileLine
reEquivHeader = EquivHeader <$ string equivHeader

-- Regex for matching patterns like (112--128)
reRange :: RE Char (Int, Int)
reRange = (,) <$> (sym '(' *> decimal <* string "--") <*> (decimal <* sym ')')

-- Regex for matching patterns like (128--192)(128--192)
reTexRect :: RE Char TexRect
reTexRect = (\(x0,x1)(y0,y1) -> TexRect x0 x1 y0 y1) <$> reRange <*> reRange

-- Regex for matching tex rect file names
reTextRectFile :: RE Char TexRect
reTextRectFile = reTexRect <* many anySym

-- Regex for matching 32 hex digits
reHash :: RE Char String
reHash = mconcat $ replicate 32 (singleton <$> psym isHexDigit)

-- Regex for matching patterns like BD3BBCAAC1743848331C500384DC4626\(72--136)(192--256)
-- the items in an equiv file
reTexSubarea :: RE Char TexSubarea
reTexSubarea = (,) <$> (reHash <* sym '\\') <*> reTexRect

-- Regex for matching any line in an equiv file
reEquivLine :: RE Char FileLine
reEquivLine = Blank <$ string ""
                <|> BeginEquiv <$ string beginEquivString
                <|> EndEquiv <$ string endEquivString
                <|> EquivElem <$> reTexSubarea
                <|> Error <$> many anySym

-- List of regex for matching the lines of an equiv file
equivFileRecogniser :: (RE Char FileLine, RE Char FileLine)
equivFileRecogniser = (reEquivHeader, reEquivLine)

-- Regex for matching the map file header line
reMapHeader :: RE Char FileLine
reMapHeader = MapHeader <$ string mapHeader

-- Regex for matching ROOT lines in map files
reRoot :: RE Char FilePath
reRoot = string "ROOT: " *> (normaliseFilePath <$> some (psym $ not . isSpace)) <* many (psym isSpace)

-- Regex for matching BASE lines in map files
reBase :: RE Char FilePath
reBase = string "BASE: " *> (normaliseFilePath <$> some (psym $ not . isSpace)) <* many (psym isSpace)

-- Regex for matching mappings in map files
reMap :: RE Char HashPath
reMap = HashPath <$> reHash <* sym ' ' <*> (normaliseFilePath <$> some (psym $ not . isSpace)) <* many (psym isSpace)

-- Regex for matching any line in a map file
reMapLine :: RE Char FileLine
reMapLine = Blank <$ string ""
                <|> Root <$> reRoot
                <|> Base <$> reBase
                <|> MapElem <$> reMap
                <|> Error <$> many anySym

-- List of regex for matching the lines of a map file
mapFileRecogniser :: (RE Char FileLine, RE Char FileLine)
mapFileRecogniser = (reMapHeader, reMapLine)


-- Apply header regex to first line and body regex to the rest
applyRecogniser :: (RE Char FileLine, RE Char FileLine) -> String -> Maybe [FileLine]
applyRecogniser (hregex, bregex) contents =
    case stripComment <$> lines contents of
        []  -> Nothing
        lns -> zipWithM ($) (match hregex : repeat (match bregex)) lns


parsePack :: IO (M.Map TexSubarea FilePath)
parsePack = do
    withCurrentDirectory "pack" $ do
        packFiles <- parsePackFiles
        let equivs = [e | PackEquiv e <- packFiles]
        let maps = [m | PackMap m <- packFiles]
        when (length equivs > 1) $ putStrLn "Found multiple equiv files"
        -- Enumerate the map paths and construct the image mappings,
        -- combining the results of each map file
        imaps <- mconcat <$> traverse imageMapsForCat maps
        -- Check the equivs
        traverse_ (\e -> case checkEquivList e of Left s -> putStrLn $ "Bad equiv: " ++ show s
                                                  Right _ -> putStrLn "Good equiv") equivs
        -- Add all the extra mappings reachable through equivalences
        let gimaps = accountForEquiv imaps $ equivFromList $ mconcat equivs
        -- Turn into a map. Map to sets of paths so that we can detect
        -- multiple mappings
        let tmap = nubOrd <$> M.fromListWith (++) [(a, [p]) | (a, p) <- gimaps]
        -- Report multiple mappings
        traverse_ (\k -> do let paths = tmap M.! k
                            when (length paths /= 1) $ putStrLn $ "Multiple mappings from " ++ show k ++ " : " ++ show paths
                  ) $ M.keys tmap
        pure $ head <$> tmap

accountForEquiv :: [(TexSubarea, FilePath)] -> Equiv TexSubarea -> [(TexSubarea, FilePath)]
accountForEquiv maps equiv = [(tsa', path) | (tsa, path) <- maps, tsa' <- listEquivalents tsa equiv]

imageMapsForCat :: MapCat -> IO [(TexSubarea, FilePath)]
imageMapsForCat (MapCat root maps) = mconcat <$> traverse (imageMapsForHashPath root) maps

imageMapsForHashPath :: FilePath -> HashPath -> IO [(TexSubarea, FilePath)]
imageMapsForHashPath root hp = do
    lrmap <- linkRedirectionsForHashPath root hp
    -- Use 'last' to ignore the link redirections and focus on the final image file. Report the
    -- ones that are missing and return the ones that are present.
    (present, missing) <- partitionM (doesFileExist . snd) $ (fmap.fmap) last lrmap
    (traverse_._2) (\path -> putStrLn $ "Missing image file: \"" ++ path ++ "\"") missing
    pure present

linkRedirectionsForHashPath :: FilePath -> HashPath -> IO [(TexSubarea, [FilePath])]
linkRedirectionsForHashPath root (HashPath h p) =
    ifM (doesDirectoryExist p)
    (do names <- listDirectory p
        -- Make a tex-subarea path pairing for each file name that starts with the form of a texrect
        let texRectPathPairs = [((h, tr), p </> n) | n <- names, Just tr <- [n =~ reTextRectFile]]
        -- For each pair, follow the links for the path.
        (traverse._2) (followLinks root) texRectPathPairs)
    (do putStrLn $ "Mapping file refers to non-existant directory: " ++ p
        pure [])


-- Paths can lead to an image or a file that contains TLNK: <new-path>. This function
-- returns the list of paths visited by following the links.
followLinks :: FilePath -> FilePath -> IO [FilePath]
followLinks r p = do exists <- doesFileExist p
                     if exists
                     then do contents <- readFile p
                             case lines contents of
                                [] -> pure [p] -- empty file, not a link
                                l:_ -> case l =~ reLink of Nothing -> pure [p] -- contents doesn't match, not a link
                                                           Just np -> fmap (p:) $ followLinks r $ r </> normaliseFilePath np
                     else pure [p] -- file doesn't exist, not a link

-- List the top-level files in the pack
packMappingFiles :: IO [FilePath]
packMappingFiles = filterM doesFileExist =<< listDirectory ""

-- Parse a file
parseFile :: FilePath -> IO (Maybe PackCat)
parseFile name = do
    parseFileContents name =<< readFile name

parseEquivFiles :: IO [EquivCat]
parseEquivFiles = do packFiles <- parsePackFiles
                     pure [e | PackEquiv e <- packFiles]

parsePackFiles :: IO [PackCat]
parsePackFiles = catMaybes <$> (traverse parseFile =<< packMappingFiles)

-- Parse a file readinto a string
-- Also pass a name for reference in error reports (ugly - needs changing)
parseFileContents :: String -> String -> IO (Maybe PackCat)
parseFileContents name contents = do
    -- Try the two recognisers - make a list of the successful results. Both can't pass, so
    -- there will be one or none. Interpret the parsed lines dependently on which file
    -- header was matched.
    case mapMaybe (`applyRecogniser` contents) [equivFileRecogniser, mapFileRecogniser] of
        []     -> do putStrLn $ "Failed to parse file: " ++ name
                     pure Nothing
        els:_  -> do putStrLn $ "Parsed file: " ++ name
                     traverse_ putStrLn $ fileErrors els name
                     pure $ case els of EquivHeader:_ -> Just $ PackEquiv $ interpretEquiv els
                                        MapHeader:_   -> Just $ PackMap $ interpretMap els
                                        _             -> Nothing


-- Return descriptions of the errors in a file.
fileErrors :: [FileLine] -> FilePath -> [String]
fileErrors ls fpath =
    let lineError (n, s) = "Bad line " ++ show n ++ " in file " ++ fpath ++ ": \"" ++ s ++ "\""
        multipleRootsError = "Map file has multiple ROOT lines: " ++ fpath
        errorLines = [(n, s) | (n, Error s) <- zip [1..] ls]
        hasMultipleRoots = length [() | Root _ <- ls] > 1
    in  [multipleRootsError | hasMultipleRoots] ++ [lineError l | l <- errorLines]

-- Collect equivalences into a list of lists and then convert to an element to set mapping
interpretEquiv ::[FileLine] -> [[TexSubarea]]
interpretEquiv ls =
    let parse = forM ls (\case {BeginEquiv  -> (do
                                                   eCurrent .= []
                                                   eOn      .= True);
                                EndEquiv    -> (do
                                                   on <- use eOn
                                                   current <- use eCurrent
                                                   when on $ eResult %= (current:)
                                                   eOn .= False);
                                EquivElem e -> (do
                                                   on <- use eOn
                                                   when on $ eCurrent %= (e:));
                                _           -> (do
                                                   return ())})
    in execState parse initEParseState ^. eResult

-- Resolve the elements path against root and base markers and then convert to a tex hash to path mapping
interpretMap :: [FileLine] -> MapCat
interpretMap ls =
    let parse = forM ls (\case {Root r                 -> (do
                                                             mRoot .= r);
                                Base b                 -> (do
                                                             root <- use mRoot
                                                             mBase .= root</>b);
                                MapElem (HashPath h p) -> (do
                                                             base <- use mBase
                                                             mResult %= (HashPath h (base</>p):));
                                _ -> return ()})
        endState = execState parse initMParseState
    in MapCat (endState^.mRoot) (endState^.mResult)

-- Strip the comment from a line if present
stripComment :: String -> String
stripComment line = fromMaybe line $ line =~ many anySym <* string "//" <* many anySym

normaliseFilePath :: String -> FilePath
normaliseFilePath = joinPath . filter (/= "") . splitOn "\\"

writeEquiv :: FilePath -> [[TexSubarea]] -> IO ()
writeEquiv path equiv = writeFile path $ equivToString equiv

equivToString :: [[TexSubarea]] -> String
equivToString e = unlines $ equivHeader : mconcat (map (\s -> ["", beginEquivString] ++ map texsubToString s ++ [endEquivString]) e)

texsubToString :: TexSubarea -> String
texsubToString (h, tr) = h ++ "\\" ++ texRectToString tr

texRectToString :: TexRect -> [Char]
texRectToString (TexRect x0 x1 y0 y1) = rangeToString x0 x1 ++ rangeToString y0 y1
rangeToString :: (Show a1, Show a2) => a1 -> a2 -> String
rangeToString x0 x1 = "(" ++ show x0 ++ "--" ++ show x1 ++ ")"
