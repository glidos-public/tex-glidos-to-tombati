module Equiv
    (
      Equiv,
      checkEquivList,
      listEquivalents,
      equivFromList,
      equivToList,
      combineEquivs
    ) where

import Data.List ( intersect )
import Data.Maybe ( fromMaybe )
import qualified Data.Map as M
import Data.Containers.ListUtils (nubOrd)

type Equiv a = M.Map a [a]

-- Check the lists are pair-wise disjoint by accumulating them
-- and checking that each new addition is disjoint with the accumulation so far
checkEquivList :: Eq a => [[a]] -> Either [a] [a]
checkEquivList = foldl (\eacc l -> do acc <- eacc
                                      let i = acc `intersect` l
                                      if null i then Right $ acc ++ l
                                                else Left i
                       ) $ Right []

-- Generate the list of elements equivalent with a given one. If the given
-- element is in the map then just lookup the list. Otherwise the element is
-- equivalent only to itself.
listEquivalents :: Ord a => a -> Equiv a -> [a]
listEquivalents v e = fromMaybe [v] (M.lookup v e)

equivFromList :: Ord a => [[a]] -> Equiv a
equivFromList = foldl (flip equate) M.empty

equivToList :: Ord a => M.Map k a -> [a]
equivToList = nubOrd . M.elems

combineEquivs :: Ord a => [[[a]]] -> [[a]]
combineEquivs = equivToList . equivFromList . mconcat

equate :: Ord a => [a] -> Equiv a -> Equiv a
equate x e = let i = x `intersect` M.keys e
             in (`M.union` e) $ fromSingle $ if null i then x
                                                       else nubOrd $ mconcat $ (`listEquivalents` e) <$> x

fromSingle :: Ord a => [a] -> Equiv a
fromSingle x = M.fromList [(e,x) | e <- x]