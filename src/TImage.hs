module TImage
    ( TImage,
      blackImage,
      scaleImageBy,
      patchImage,
      readImage,
      writeImage,
      cyanToTransparent,
      unpremulitply
    ) where

import qualified Graphics.Image as G
import qualified Graphics.Image.Interface as G

import Pack ( TexRect(TexRect) )

type TImage = G.Image G.VS G.RGBA Double

blackImage :: Int -> Int -> TImage
blackImage x y = G.makeImageR G.VS (y, x) (const (G.PixelRGBA 0 0 0 0 :: G.Pixel G.RGBA Double))

scaleImageBy :: Double -> Double -> TImage -> TImage
scaleImageBy x y = G.scale G.Bilinear G.Edge (y,x)

patchImage :: TImage -> (TexRect, TImage) -> TImage
patchImage image (rect, patchImg) = let TexRect xmin xmax ymin ymax = rect
                                        width = 4 * (xmax - xmin)
                                        height = 4 * (ymax - ymin)
                                        sizedPatchImage = G.resize G.Bilinear G.Edge (height,width) patchImg
                                    in G.superimpose (4 * ymin, 4 * xmin) sizedPatchImage image

readImage :: FilePath -> IO (Maybe TImage)
readImage path = do
    i <- G.readImage path
    case i of Left s -> do putStrLn $ "Failed to load image: " ++ path ++ " (error: " ++ s ++ ")"
                           pure Nothing
              Right im -> pure $ Just im

writeImage :: FilePath -> TImage -> IO ()
writeImage f = G.writeImageExact G.PNG [] f . G.map (G.toWord8 <$>)

cyanToTransparent :: TImage -> TImage
cyanToTransparent = G.map $ G.fromComponents . (\p@(r,g,b,_) -> if r == 0.0 && g == 1.0 && b == 1.0
                                                                then (0.0,0.0,0.0,0.0)
                                                                else p) . G.toComponents

unpremulitply :: TImage -> TImage
unpremulitply =  G.map $ G.fromComponents . (\p@(r,g,b,a) -> if a > 0.001
                                                             then (r/a,g/a,b/a,a)
                                                             else p) . G.toComponents