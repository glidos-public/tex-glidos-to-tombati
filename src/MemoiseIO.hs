module MemoiseIO
(
    memoise,
    memoiseEval
) where

import Data.Map as M ( insert, lookup, Map, empty )
import Control.Monad.State.Lazy
    ( StateT, MonadTrans(lift), MonadState(get), modify, evalStateT )


memoise :: Ord a => (a -> IO b) -> a -> StateT (Map a b) IO b
memoise f x =
    do
        m <- get
        case M.lookup x m of Just v  -> return v
                             Nothing -> do  v <- lift $ f x
                                            modify $ insert x v
                                            return v

memoiseEval :: StateT (Map a b) IO c -> IO c
memoiseEval op = evalStateT op M.empty
