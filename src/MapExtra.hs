module MapExtra
    ( catMaybes,
      MapExtra.curry
    ) where

import Data.Map
    ( fromList, fromListWith, mapMaybe, toList, Map )

catMaybes :: Map a (Maybe b) -> Map a b
catMaybes = mapMaybe id

curry :: (Ord a, Ord b) => Map (a,b) c -> Map a (Map b c)
curry pmap = fromList <$> fromListWith (++) [(x,[(y,z)]) | ((x,y),z) <- toList pmap]